/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.removeelement;

import java.util.Arrays;

/**
 *
 * @author User
 */
public class RemoveElement {
    public static int removeElement(int[] nums, int val) {
        int k = 0; // จำนวนสมาชิกใน nums ที่ไม่เท่ากับ val

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[k] = nums[i];
                k++;
            }
        }

        return k;
    }

    public static void main(String[] args) {
        int[] nums = {0, 1, 2, 3,0,4,2};
        int val = 2;

        int k = removeElement(nums, val);

        System.out.println("Updated Array: " + Arrays.toString(nums));
        System.out.println("Number of elements not equal to val: " + k);
    }
}

